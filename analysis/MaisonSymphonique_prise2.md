# IRs
Measured at Maison Symphonique with the Sinesweep method

IR_Iph07-session3.17_s4_mono
<audio src="IR_Iph07-session3.17_s4_mono.wav" controls> <a href="IR_Iph07-session3.17_s4_mono.wav">Download</a></audio>

IR_Iph09-session4.5_s11_mono
<audio src="IR_Iph09-session4.5_s11_mono.wav" controls> <a href="IR_Iph09-session4.5_s11_mono.wav">Download</a></audio>

IR_Iph17-Session1.13_S3_mono
<audio src="IR_Iph17-Session1.13_S3_mono.wav" controls> <a href="IR_Iph17-Session1.13_S3_mono.wav">Download</a></audio>

IR_Iph21-session2.41_S02_mono
<audio src="IR_Iph21-session2.41_S02_mono.wav" controls> <a href="IR_Iph21-session2.41_S02_mono.wav">Download</a></audio>


# Audio Book Auralization

Non auralized file
<audio src="audio_book_src_BWAV_24bit_Export.wav" controls> <a href="audio_book_src_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph07-session3.17_s4_mono
<audio src="audio_book_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav" controls> <a href="audio_book_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph09-session4.5_s11_mono
<audio src="audio_book_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav" controls> <a href="audio_book_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph17-Session1.13_S3_mono
<audio src="audio_book_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav" controls> <a href="audio_book_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph21-session2.41_S02_mono
<audio src="audio_book_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav" controls> <a href="audio_book_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav">Download</a></audio>

# Gunshot

Non auralized file
<audio src="gunshot_src_BWAV_24bit_Export.wav" controls> <a href="gunshot_src_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph07-session3.17_s4_mono
<audio src="gunshot_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav" controls> <a href="gunshot_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph09-session4.5_s11_mono
<audio src="gunshot_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav" controls> <a href="gunshot_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph17-Session1.13_S3_mono
<audio src="gunshot_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav" controls> <a href="gunshot_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph21-session2.41_S02_mono
<audio src="gunshot_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav" controls> <a href="gunshot_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav">Download</a></audio>

# Piano

Non auralized file
<audio src="piano_src_BWAV_24bit_Export.wav" controls> <a href="piano_src_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph07-session3.17_s4_mono
<audio src="piano_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav" controls> <a href="piano_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph09-session4.5_s11_mono
<audio src="piano_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav" controls> <a href="piano_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph17-Session1.13_S3_mono
<audio src="piano_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav" controls> <a href="piano_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph21-session2.41_S02_mono
<audio src="piano_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav" controls> <a href="piano_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav">Download</a></audio>

# Violon

Non auralized file
<audio src="violon_src_BWAV_24bit_Export.wav" controls> <a href="violon_src_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph07-session3.17_s4_mono
<audio src="violon_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav" controls> <a href="violon_IR_Iph07-session3.17_s4_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph09-session4.5_s11_mono
<audio src="violon_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav" controls> <a href="violon_IR_Iph09-session4.5_s11_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph17-Session1.13_S3_mono
<audio src="violon_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav" controls> <a href="violon_IR_Iph17-Session1.13_S3_mono_BWAV_24bit_Export.wav">Download</a></audio>

IR_Iph21-session2.41_S02_mono
<audio src="violon_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav" controls> <a href="violon_IR_Iph21-session2.41_S02_mono_BWAV_24bit_Export.wav">Download</a></audio>
