# From the Sinesweep set

Zylia ZM-1 and generated using a Sinesweep method

</a> <audio src="RI-Zylia-Sato-step3-low.wav" controls>
<a href="RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>

# From the MLS set

45g (close)  

<audio src="RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
 <a href="RI-45g-StageFRmic-P1sceneSpeak.wav">Download</a>
</audio>

45g (far)  

<audio src="RI-45g-PlanBACKL-P1sceneSpeak.wav" controls>
 <a href="RI-45g-PlanBACKL-P1sceneSpeak.wav">Download</a>
</audio>

Zylia (close)

<audio src="RI-zylia-V1-P1-scene.wav" controls>
 <a href="RI-zylia-V1-P1-scene.wav">Download</a>
</audio>


# Denoised from the MLS set

Using Audacity denoiser with the following parameters:

* Noise reduction (dB): 30
* Sensitivity: 10.5
* Frequency Smoothing (bands): 3


45g (far)

<audio src="RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
 <a href="RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav">Download</a>
</audio>

Zylia (close)

<audio src="RI-zylia-V1-P1-scene-denoised.wav" controls>
 <a href="RI-zylia-V1-P1-scene-denoised.wav">Download</a>
</audio>


