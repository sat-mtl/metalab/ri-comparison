We compare two sets of measured Impulse Responses, the MLS set and the Sine Sweep set.

The MLS set contains noise and has been measured with two different models of microphone, named 45g and Zylia. We use only one from the second set (the Sine Sweep set), captured with the same Zylia microphone.

The goal of this page is to perform a subjective evaluation of the impact of the noise in the MLS set.

The sine sweep set has been capture in the SAT's Satosphère, while the MLS set measured a larger space. Accordingly, impulse responses from the MLS set are longer. 

The convolutions has been computer with jconvolver.

# IRs comparison

The resulting Impulse Responses in the MLS set has two issues:

* a significant background noise
* small pre and post echos

As it can be heard hereafter, these issues are not present the Sine Sweep set, although some has been captured with the same microphone (Zylia ZM-1) :

Sinesweep Zylia ZM-1

</a> <audio src="IRs/RI-Zylia-Sato-step3-low.wav" controls>
<a href="IRs/RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>

MLS 45g (close)  

<audio src="IRs/RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
 <a href="IRs/RI-45g-StageFRmic-P1sceneSpeak.wav">Download</a>
</audio>


MLS Zylia (close)

<audio src="IRs/RI-zylia-V1-P1-scene.wav" controls>
 <a href="IRs/RI-zylia-V1-P1-scene.wav">Download</a>
</audio>


# Correctness of the Zylia Microphone for convolution 

Using a human reader capture, we can be convinced the use of the Sine Sweep Impulse response simulates appropriately the acoustic effect of the Satosphere Dome.

Source audio

</a> <audio src="samples/audio_book_src.wav" controls>
<a href="samples/audio_book_src.wav"> Download </a>
</audio>

Convolution with the Sine Sweep impulse response

</a> <audio src="samples/audio_book_RI-Zylia-Sato-step3-low.wav" controls>
<a href="samples/audio_book_RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>


# Effect of noise

The noise in the MLS impulse response generation extra noise and produce non natural equalization with the convolution.

Source audio

</a> <audio src="samples/audio_book_src.wav" controls>
<a href="samples/audio_book_src.wav"> Download </a>
</audio>

Convolution using MLS 45g (close)  

</a> <audio src="samples/audio_book_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="samples/audio_book_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>

The noise is producing unnatural sustain with violin sounds, while it sound natural with the Sine Sweep measure:

Source audio

</a> <audio src="samples/violon-anechoic_src.wav" controls>
<a href="samples/violon-anechoic_src.wav"> Download </a>
</audio>

MLS 45g (close)  

</a> <audio src="samples/violon-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="samples/violon-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>

However, it sounds more natural with the Zylia Sine Sweep IR

Zylia Sine Sweep

</a> <audio src="samples/violon-anechoic_RI-Zylia-Sato-step3-low.wav" controls>
<a href="samples/violon-anechoic_RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>


The Echo in the MLS set is particularly problematic with long piano notes since several attacks can be noticed:

Source audio

</a> <audio src="samples/piano-anechoic_src.wav" controls>
<a href="samples/piano-anechoic_src.wav"> Download </a>
</audio>

MLS 45g (close)  

</a> <audio src="samples/piano-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="samples/piano-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>

# Denoising the MLS Impulse response

Using Audacity denoiser with the following parameters:

* Noise reduction (dB): 30
* Sensitivity: 10.5
* Frequency Smoothing (bands): 3

Original 45g (far)  

<audio src="IRs/RI-45g-PlanBACKL-P1sceneSpeak.wav" controls>
 <a href="IRs/RI-45g-PlanBACKL-P1sceneSpeak.wav">Download</a>
</audio>

Denoised 45g (far)

<audio src="IRs/RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
 <a href="IRs/RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav">Download</a>
</audio>


Original Zylia (close)

<audio src="IRs/RI-zylia-V1-P1-scene.wav" controls>
 <a href="IRs/RI-zylia-V1-P1-scene.wav">Download</a>
</audio>

Denoised Zylia (close)

<audio src="IRs/RI-zylia-V1-P1-scene-denoised.wav" controls>
 <a href="IRs/RI-zylia-V1-P1-scene-denoised.wav">Download</a>
</audio>


# Use of denoised MLS IRs

The tail of the reverb is not as smooth as in the actual space

Source audio (Gunshot in an anechoic room)

</a> <audio src="samples/gunshotanechoic_src.wav" controls>
<a href="samples/gunshotanechoic_src.wav"> Download </a>
</audio>

MLS denoised 45g (far)

</a> <audio src="samples/gunshotanechoic_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
<a href="samples/gunshotanechoic_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav"> Download </a>
</audio>

MLS denoised Zylia (close)

</a> <audio src="samples/gunshotanechoic_RI-zylia-V1-P1-scene-denoised.wav" controls>
<a href="samples/gunshotanechoic_RI-zylia-V1-P1-scene-denoised.wav"> Download </a>
</audio>


Convolutions still sounds muffled. This can be heard in violin and voice.

Source audio (Violin)

</a> <audio src="samples/violon-anechoic_src.wav" controls>
<a href="samples/violon-anechoic_src.wav"> Download </a>
</audio>

MLS denoised Zylia (close)

</a> <audio src="samples/violon-anechoic_RI-zylia-V1-P1-scene-denoised.wav" controls>
<a href="samples/violon-anechoic_RI-zylia-V1-P1-scene-denoised.wav"> Download </a>
</audio>

Reader source audio

</a> <audio src="samples/audio_book_src.wav" controls>
<a href="samples/audio_book_src.wav"> Download </a>
</audio>

MLS denoised 45g (far)

</a> <audio src="samples/audio_book_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
<a href="samples/audio_book_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav"> Download </a>
</audio>

MLS Zylia (close)

</a> <audio src="samples/audio_book_RI-zylia-V1-P1-scene.wav" controls>
<a href="samples/audio_book_RI-zylia-V1-P1-scene.wav"> Download </a>
</audio>
