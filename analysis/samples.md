# Reader (audio book)

Source audio

</a> <audio src="audio_book_src.wav" controls>
<a href="audio_book_src.wav"> Download </a>
</audio>


Zylia Sinesweep

</a> <audio src="audio_book_RI-Zylia-Sato-step3-low.wav" controls>
<a href="audio_book_RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>


MLS 45g (close)  

</a> <audio src="audio_book_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="audio_book_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>


MLS 45g (far)  

</a> <audio src="audio_book_RI-45g-PlanBACKL-P1sceneSpeak.wav" controls>
<a href="audio_book_RI-45g-PlanBACKL-P1sceneSpeak.wav"> Download </a>
</audio>


MLS Zylia (close)

</a> <audio src="audio_book_RI-zylia-V1-P1-scene.wav" controls>
<a href="audio_book_RI-zylia-V1-P1-scene.wav"> Download </a>
</audio>


MLS denoised 45g (far)

</a> <audio src="audio_book_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
<a href="audio_book_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav"> Download </a>
</audio>


MLS denoised Zylia (close)

</a> <audio src="audio_book_RI-zylia-V1-P1-scene-denoised.wav" controls>
<a href="audio_book_RI-zylia-V1-P1-scene-denoised.wav"> Download </a>
</audio>



# Gunshot in anechoic chamber

Source audio

</a> <audio src="gunshotanechoic_src.wav" controls>
<a href="gunshotanechoic_src.wav"> Download </a>
</audio>


Zylia Sinesweep

</a> <audio src="gunshotanechoic_RI-Zylia-Sato-step3-low.wav" controls>
<a href="gunshotanechoic_RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>


MLS 45g (close)  

</a> <audio src="gunshotanechoic_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="gunshotanechoic_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>


MLS 45g (far)  

</a> <audio src="gunshotanechoic_RI-45g-PlanBACKL-P1sceneSpeak.wav" controls>
<a href="gunshotanechoic_RI-45g-PlanBACKL-P1sceneSpeak.wav"> Download </a>
</audio>


MLS Zylia (close)

</a> <audio src="gunshotanechoic_RI-zylia-V1-P1-scene.wav" controls>
<a href="gunshotanechoic_RI-zylia-V1-P1-scene.wav"> Download </a>
</audio>


MLS denoised 45g (far)

</a> <audio src="gunshotanechoic_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
<a href="gunshotanechoic_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav"> Download </a>
</audio>


MLS denoised Zylia (close)

</a> <audio src="gunshotanechoic_RI-zylia-V1-P1-scene-denoised.wav" controls>
<a href="gunshotanechoic_RI-zylia-V1-P1-scene-denoised.wav"> Download </a>
</audio>




# Piano in anechoic chamber

Source audio

</a> <audio src="piano-anechoic_src.wav" controls>
<a href="piano-anechoic_src.wav"> Download </a>
</audio>


Zylia Sinesweep

</a> <audio src="piano-anechoic_RI-Zylia-Sato-step3-low.wav" controls>
<a href="piano-anechoic_RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>


MLS 45g (close)  

</a> <audio src="piano-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="piano-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>


MLS 45g (far)  

</a> <audio src="piano-anechoic_RI-45g-PlanBACKL-P1sceneSpeak.wav" controls>
<a href="piano-anechoic_RI-45g-PlanBACKL-P1sceneSpeak.wav"> Download </a>
</audio>


MLS Zylia (close)

</a> <audio src="piano-anechoic_RI-zylia-V1-P1-scene.wav" controls>
<a href="piano-anechoic_RI-zylia-V1-P1-scene.wav"> Download </a>
</audio>


MLS denoised 45g (far)

</a> <audio src="piano-anechoic_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav" controls>
<a href="piano-anechoic_RI-45g-PlanBACKL-P1sceneSpeak-denoised.wav"> Download </a>
</audio>


MLS denoised Zylia (close)

</a> <audio src="piano-anechoic_RI-zylia-V1-P1-scene-denoised.wav" controls>
<a href="piano-anechoic_RI-zylia-V1-P1-scene-denoised.wav"> Download </a>
</audio>




# Violin in anechoic chamber

Source audio

</a> <audio src="violon-anechoic_src.wav" controls>
<a href="violon-anechoic_src.wav"> Download </a>
</audio>


Zylia Sinesweep

</a> <audio src="violon-anechoic_RI-Zylia-Sato-step3-low.wav" controls>
<a href="violon-anechoic_RI-Zylia-Sato-step3-low.wav"> Download </a>
</audio>


MLS 45g (close)  

</a> <audio src="violon-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav" controls>
<a href="violon-anechoic_RI-45g-StageFRmic-P1sceneSpeak.wav"> Download </a>
</audio>


MLS 45g (far)  

</a> <audio src="violon-anechoic_RI-45g-PlanBACKL-P1sceneSpeak.wav" controls>
<a href="violon-anechoic_RI-45g-PlanBACKL-P1sceneSpeak.wav"> Download </a>
</audio>


MLS Zylia (close)

</a> <audio src="violon-anechoic_RI-zylia-V1-P1-scene.wav" controls>
<a href="violon-anechoic_RI-zylia-V1-P1-scene.wav"> Download </a>
</audio>


MLS denoised 45g (far)

</a> <audio src="" controls>
<a href=""> Download </a>
</audio>


MLS denoised Zylia (close)

</a> <audio src="violon-anechoic_RI-zylia-V1-P1-scene-denoised.wav" controls>
<a href="violon-anechoic_RI-zylia-V1-P1-scene-denoised.wav"> Download </a>
</audio>

